import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IBook } from '@shared/models/book.model';
import { BookListService } from '@shared/services/book-list.service';
import { BookshelfService } from '@shared/services/bookshelf.service';
import { Observable, shareReplay, switchMap } from 'rxjs';

@Component({
  selector: 'app-book-details',
  templateUrl: './book-details.component.html',
  styleUrl: './book-details.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BookDetailsComponent implements OnInit {
  book$: Observable<IBook> = this.activatedRoute.params.pipe(
    switchMap((p) => this.bookListService.getDetails(p['id'])),
    shareReplay(1),
  );

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly bookListService: BookListService,
    public bookshelfService: BookshelfService,
  ) {}

  ngOnInit(): void {}
}
