import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { RouterModule, Routes } from '@angular/router';
import { ListAuthorsPipe } from '@core/pipes/list-authors.pipe';
import { AddToBookshelfComponent } from '@shared/components/add-to-bookshelf/add-to-bookshelf.component';
import { LoaderDirective } from '@shared/directives/loader.directive';
import { BookDetailsComponent } from './book-details.component';

const routes: Routes = [
  {
    path: '',
    component: BookDetailsComponent,
  },
];

@NgModule({
  declarations: [BookDetailsComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatCardModule,
    ListAuthorsPipe,
    MatIconModule,
    MatButtonModule,
    MatTooltipModule,
    AddToBookshelfComponent,
    LoaderDirective,
  ],
  providers: [],
})
export class BookDetailsModule {}
