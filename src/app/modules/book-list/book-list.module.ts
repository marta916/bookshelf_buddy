import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatError, MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatTooltipModule } from '@angular/material/tooltip';
import { RouterModule, Routes } from '@angular/router';
import { ListAuthorsPipe } from '@core/pipes/list-authors.pipe';
import { LoaderDirective } from '@shared/directives/loader.directive';
import { BookListService } from '@shared/services/book-list.service';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { BookListComponent } from './book-list.component';
import { BookCardComponent } from './components/book-card/book-card.component';

const routes: Routes = [
  {
    path: '',
    component: BookListComponent,
  },
];

@NgModule({
  declarations: [BookListComponent, BookCardComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatCardModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatError,
    MatTooltipModule,
    ListAuthorsPipe,
    InfiniteScrollModule,
    LoaderDirective,
    MatIconModule,
    MatButtonModule,
  ],
  providers: [BookListService],
})
export class BookListModule {}
