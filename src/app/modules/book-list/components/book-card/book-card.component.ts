import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { IBook } from '@shared/models/book.model';
import { BookshelfService } from '@shared/services/bookshelf.service';

@Component({
  selector: 'app-book-card',
  templateUrl: './book-card.component.html',
  styleUrl: './book-card.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BookCardComponent {
  @Input('data') data!: IBook;

  constructor(public bookshelfService: BookshelfService) {}
}
