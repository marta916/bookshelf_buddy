import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  OnInit,
} from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { IBook } from '@shared/models/book.model';
import { IFilters } from '@shared/models/filters.model';
import { IResult } from '@shared/models/result.model';
import { BookListService } from '@shared/services/book-list.service';
import {
  BehaviorSubject,
  Observable,
  debounceTime,
  distinctUntilChanged,
  filter,
  forkJoin,
  take,
  tap,
} from 'rxjs';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrl: './book-list.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BookListComponent implements OnInit, AfterViewInit {
  observableItemsArray: BehaviorSubject<IBook[] | null> = new BehaviorSubject<
    IBook[] | null
  >(null);

  items$: Observable<IBook[] | null> = this.observableItemsArray.asObservable();

  batchMaxCount = 32;
  totalCount = 0;
  filters: IFilters = { searchValue: '', page: 0 };

  searchFormControl = new FormControl('', [Validators.minLength(3)]);

  constructor(private bookListService: BookListService) {}

  ngOnInit(): void {
    this.getBooks();
  }

  ngAfterViewInit(): void {
    this.searchFormControl.valueChanges
      .pipe(
        debounceTime(500),
        distinctUntilChanged(),
        filter(() => this.searchFormControl.valid),
        tap(() => {
          this.filters.searchValue = this.searchFormControl.value;

          this.observableItemsArray.next(null);
          this.getBooks(true);
        }),
      )
      .subscribe();
  }

  public getBooks(clear: boolean = false): void {
    if (clear) {
      this.filters.page = 0;
      this.totalCount = 0;
    }

    if (
      this.totalCount &&
      this.batchMaxCount * this.filters.page >= this.totalCount
    ) {
      return;
    }

    this.filters.page++;

    forkJoin([
      this.items$.pipe(take(1)),
      this.bookListService.getResult(this.filters),
    ]).subscribe((data: [IBook[] | null, IResult]) => {
      this.totalCount = data[1] ? data[1].totalCount : 0;
      let newData = data[1] ? data[1].books || [] : [];

      this.observableItemsArray.next(
        clear || !data[0] ? newData : data[0].concat(newData),
      );
    });
    // TODO: Obsłużyć możliwe błędy z API
  }
}
