import { Component, OnInit } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { IBookOnShelf } from '@shared/models/book-on-shelf.model';
import { BookLocalStorageService } from '@shared/services/book-localstorage.service';
import { BookshelfService } from '@shared/services/bookshelf.service';
import { Observable, filter } from 'rxjs';

@Component({
  selector: 'app-bookshelf',
  templateUrl: './bookshelf.component.html',
  styleUrl: './bookshelf.component.scss',
})
export class BookshelfComponent implements OnInit {
  books: IBookOnShelf[] = [];

  reloadAfterDelete$: Observable<boolean> =
    this.bookshelfService.subDeletedMsg();

  reloadAfterDeleteSub = this.reloadAfterDelete$
    .pipe(
      filter((msg) => !!msg),
      takeUntilDestroyed(),
    )
    .subscribe(() => this.loadBooks());

  constructor(
    private readonly bookLocalStorageService: BookLocalStorageService,
    private readonly bookshelfService: BookshelfService,
  ) {}

  ngOnInit(): void {
    this.loadBooks();
  }

  private loadBooks(): void {
    this.books = this.bookLocalStorageService.getBooks();
  }
}
