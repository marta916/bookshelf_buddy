import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatTooltipModule } from '@angular/material/tooltip';
import { RouterModule, Routes } from '@angular/router';
import { ListAuthorsPipe } from '@core/pipes/list-authors.pipe';
import { LoaderDirective } from '@shared/directives/loader.directive';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { BookshelfComponent } from './bookshelf.component';
import { BookshelfCardComponent } from './components/bookshelf-card/bookshelf-card.component';

const routes: Routes = [
  {
    path: '',
    component: BookshelfComponent,
  },
];

@NgModule({
  declarations: [BookshelfComponent, BookshelfCardComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatCardModule,
    ListAuthorsPipe,
    InfiniteScrollModule,
    LoaderDirective,
    MatTooltipModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
  ],
  providers: [],
})
export class BookshelfModule {}
