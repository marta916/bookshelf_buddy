import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { BookStatus } from '@shared/enum/book-status.enum';
import { IBookOnShelf } from '@shared/models/book-on-shelf.model';
import { BookshelfService } from '@shared/services/bookshelf.service';

@Component({
  selector: 'app-bookshelf-card',
  templateUrl: './bookshelf-card.component.html',
  styleUrl: './bookshelf-card.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BookshelfCardComponent implements OnInit {
  @Input('data') data!: IBookOnShelf;

  status: string = '';

  constructor(private readonly bookshelfService: BookshelfService) {}

  ngOnInit(): void {
    this.status = BookStatus[this.data.status as keyof typeof BookStatus];
  }

  deleteFromBookshelf(bookId: string): void {
    this.bookshelfService.openDeleteDialog(bookId);
  }
}
