import { Component } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styles:
    'mat-toolbar {background-color: #fce762; color: #3f51b5;}.example-spacer {flex: 1 1 auto;}',
  standalone: true,
  imports: [MatToolbarModule, MatIconModule, MatButtonModule, MatTooltipModule],
})
export class ToolbarComponent {}
