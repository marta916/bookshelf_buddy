import { Pipe, PipeTransform } from '@angular/core';
import { IAuthor } from '@shared/models/author.model';

@Pipe({
  name: 'listAuthors',
  standalone: true,
})
export class ListAuthorsPipe implements PipeTransform {
  transform(value: IAuthor[]): string {
    return value.map((author) => author.name).join('; ');
  }
}
