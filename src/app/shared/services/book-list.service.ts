import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IServerBookDto } from '@shared/dto/server-book.dto';
import { IServerResultDto } from '@shared/dto/server-result.dto';
import { ConvertServerResponseHelper } from '@shared/helper/convert-server-response.helper';
import { FilterToQueryHelper } from '@shared/helper/filter-to-query.helper';
import { IBook } from '@shared/models/book.model';
import { IFilters } from '@shared/models/filters.model';
import { IResult } from '@shared/models/result.model';
import { Observable, map, switchMap } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class BookListService {
  constructor(private http: HttpClient) {}

  getResult(filters: IFilters): Observable<IResult> {
    return this.http
      .get<IServerResultDto>(FilterToQueryHelper.createQuery(filters))
      .pipe(
        map((res: IServerResultDto) =>
          ConvertServerResponseHelper.convertAllServerResponse(res),
        ),
      );
  }

  getDetails(id: number): Observable<IBook> {
    return this.http
      .get<IServerBookDto>(`/${id.toString()}`)
      .pipe(
        switchMap((sb: IServerBookDto) =>
          ConvertServerResponseHelper.convertServerResponse([sb]),
        ),
      );
  }
}
