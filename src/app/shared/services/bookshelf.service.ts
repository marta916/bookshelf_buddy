import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AddToBookshelfComponent } from '@shared/components/add-to-bookshelf/add-to-bookshelf.component';
import { DeleteFromBookshelfComponent } from '@shared/components/delete-from-bookshelf/delete-from-bookshelf.component';
import { IBookOnShelf } from '@shared/models/book-on-shelf.model';
import { IBook } from '@shared/models/book.model';
import { Observable, Subject, filter, tap } from 'rxjs';
import { BookLocalStorageService } from './book-localstorage.service';

@Injectable({
  providedIn: 'root',
})
export class BookshelfService {
  private isDeleted: Subject<boolean> = new Subject<boolean>();

  constructor(
    public dialog: MatDialog,
    private readonly bookLocalStorageService: BookLocalStorageService,
    private readonly snackBar: MatSnackBar,
  ) {}

  sendDeletedMsg() {
    this.isDeleted.next(true);
  }

  subDeletedMsg(): Observable<boolean> {
    return this.isDeleted.asObservable();
  }

  openAddDialog(book: IBook): void {
    const dialogRef = this.dialog.open(AddToBookshelfComponent, {
      data: book,
      height: '500px',
      width: '600px',
    });

    dialogRef
      .afterClosed()
      .pipe(
        filter((result: { notes: string | null; status: string }) => !!result),
        tap((result: { notes: string | null; status: string }) => {
          // TODO: wystarczyłoby zapisanie id książki i pobranie jej w widoku półki ale wymagałoby to większej obróbki danych przy pobieraniu
          const newBook: IBookOnShelf = {
            bookId: book.id.toString(),
            notes: result.notes,
            status: result.status,
            bookData: book,
          };

          // TODO: sprawdzanie czy dana książka już jest "na półce" zeby nie wrzucać tego samego kilka razy
          this.bookLocalStorageService.setBook(newBook);
          this.snackBar.open('Book added to your Bookshelf!', undefined, {
            duration: 3000,
          });
        }),
      )
      .subscribe();
  }

  openDeleteDialog(bookId: string): void {
    const dialogRef = this.dialog.open(DeleteFromBookshelfComponent, {
      height: '200px',
      width: '600px',
    });

    dialogRef
      .afterClosed()
      .pipe(
        filter((result: boolean | undefined) => !!result),
        tap(() => {
          this.bookLocalStorageService.removeBook(bookId);
          this.snackBar.open('Book deleted from your Bookshelf!', undefined, {
            duration: 3000,
          });

          this.sendDeletedMsg();
        }),
      )
      .subscribe();
  }
}
