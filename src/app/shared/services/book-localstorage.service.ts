import { Injectable } from '@angular/core';
import { IBookOnShelf } from '@shared/models/book-on-shelf.model';

@Injectable({
  providedIn: 'root',
})
export class BookLocalStorageService {
  private readonly BOOKSHELF_KEY = 'BOOKSHELF_KEY';

  constructor() {}

  setBook(value: IBookOnShelf): void {
    const list: IBookOnShelf[] = this.getBooks();
    list.push(value);
    this.setBooks(list);
  }

  getBook(bookId: string): IBookOnShelf | undefined {
    return this.getBooks().find((el: IBookOnShelf) => el.bookId === bookId);
  }

  setBooks(list: IBookOnShelf[]): void {
    localStorage.setItem(this.BOOKSHELF_KEY, this.getStringValue(list));
  }

  getBooks(): IBookOnShelf[] {
    return this.getParsedValue(localStorage.getItem(this.BOOKSHELF_KEY));
  }

  removeBook(bookId: string): void {
    const list: IBookOnShelf[] = this.getBooks();
    const foundIndex = list.findIndex(
      (el: IBookOnShelf) => el.bookId === bookId,
    );

    if (foundIndex >= 0) {
      list.splice(foundIndex, 1);
    }

    this.setBooks(list);
  }

  clear(): void {
    localStorage.clear();
  }

  private getParsedValue(stringValue: string | null): IBookOnShelf[] {
    return JSON.parse(stringValue || '[]');
  }

  private getStringValue(parsedValue: IBookOnShelf[]): string {
    return JSON.stringify(parsedValue);
  }
}
