import {
  Directive,
  EmbeddedViewRef,
  Input,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';
import { LoaderComponent } from '@core/components/loader/loader.component';

interface LoaderContext {
  appLoader: unknown;
}

@Directive({
  standalone: true,
  selector: '[appLoader]',
})
export class LoaderDirective {
  private view?: EmbeddedViewRef<LoaderContext>;

  constructor(
    private templateRef: TemplateRef<LoaderContext>,
    private viewContainer: ViewContainerRef,
  ) {}

  @Input() set appLoader(data: unknown) {
    if (data && !this.view) {
      this.viewContainer.clear();
      this.view = this.viewContainer.createEmbeddedView<LoaderContext>(
        this.templateRef,
        {
          appLoader: data,
        },
      );
    } else if (!data) {
      this.view = undefined;
      this.viewContainer.clear();
      this.viewContainer.createComponent(LoaderComponent);
    } else if (data && this.view) {
      Object.assign(this.view.context, { appLoader: data });
    }
  }
}
