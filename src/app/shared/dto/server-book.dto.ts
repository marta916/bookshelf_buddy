import { IServerFormatDto } from './server-format.dto';
import { IServerPersonDto } from './server-person.dto';

export interface IServerBookDto {
  id: number;
  title: string;
  subjects: string[];
  authors: IServerPersonDto[];
  translators: IServerPersonDto[];
  bookshelves: string[];
  languages: string[];
  copyright: boolean | null;
  media_type: string;
  formats: IServerFormatDto;
  download_count: number;
}
