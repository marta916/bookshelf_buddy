import { IServerBookDto } from './server-book.dto';

export interface IServerResultDto {
  count: number;
  next: string | null;
  previous: string | null;
  results: IServerBookDto[];
}
