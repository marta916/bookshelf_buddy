export interface IServerFormatDto {
  [key: string]: string;
}
