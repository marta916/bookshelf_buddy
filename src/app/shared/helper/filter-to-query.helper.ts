import { IFilters } from '@shared/models/filters.model';

export class FilterToQueryHelper {
  public static createQuery(filters: IFilters): string {
    const searchParams = new URLSearchParams();
    filters.page && searchParams.append('page', filters.page.toString());
    filters.searchValue && searchParams.append('search', filters.searchValue);
    return `?${searchParams.toString()}`;
  }
}
