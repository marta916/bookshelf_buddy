import { IServerBookDto } from '@shared/dto/server-book.dto';
import { IServerFormatDto } from '@shared/dto/server-format.dto';
import { IServerResultDto } from '@shared/dto/server-result.dto';
import { IAuthor } from '@shared/models/author.model';
import { IBook } from '@shared/models/book.model';
import { IResult } from '@shared/models/result.model';

export class ConvertServerResponseHelper {
  public static convertAllServerResponse(
    serverResult: IServerResultDto,
  ): IResult {
    return {
      totalCount: serverResult.count,
      books: this.convertServerResponse(serverResult.results),
    } as IResult;
  }

  public static convertServerResponse(serverBooks: IServerBookDto[]): IBook[] {
    return serverBooks.map(
      (sb): IBook => ({
        id: sb.id,
        title: sb.title,
        authors: sb.authors.map(
          (a): IAuthor => ({
            name: a.name,
            birthYear: a.birth_year,
            deathYear: a.death_year,
          }),
        ),
        cover: this.getCoverUrl(sb.formats),
      }),
    );
  }

  private static getCoverUrl(formats: IServerFormatDto): string | null {
    let imageKey = Object.keys(formats).find(
      (key: string) => key === 'image/jpeg',
    );
    return imageKey ? formats[imageKey] : null;
  }
}
