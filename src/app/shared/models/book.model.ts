import { IAuthor } from './author.model';

export interface IBook {
  id: number;
  title: string;
  authors: IAuthor[];
  cover: string | null;

  /**
   * TODO: Poniższe pola też można użyć w apce.
   * Można dodatkowo filtrować listę po subjects i bookshelves (które chciałabym również móc definiować sama).
   * Z obiektu Formats można wybrać URLe do fragmentów tekstu książki. Można by je wylistować w szczegółach a w niektórych przypadkach może i wyświetlić treść w szczegółach książki.
   */
  // subjects: string[];
  // bookshelves: string[];
  // download_count: number;
  // formats: Format;
}
