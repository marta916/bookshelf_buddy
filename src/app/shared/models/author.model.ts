export interface IAuthor {
  birthYear: number | null;
  deathYear: number | null;
  name: string;
}
