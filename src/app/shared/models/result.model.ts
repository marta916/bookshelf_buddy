import { IBook } from './book.model';

export interface IResult {
  books: IBook[];
  totalCount: number;
}
