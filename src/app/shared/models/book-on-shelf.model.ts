import { IBook } from './book.model';

export interface IBookOnShelf {
  bookId: string;
  notes: string | null;
  status: string;
  bookData: IBook;
}
