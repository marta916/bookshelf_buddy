export interface IFilters {
  searchValue: string | null;
  page: number;
}
