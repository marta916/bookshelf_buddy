import { CommonModule } from '@angular/common';
import { Component, Inject } from '@angular/core';
import {
  FormControl,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import {
  MAT_DIALOG_DATA,
  MatDialogActions,
  MatDialogClose,
  MatDialogContent,
  MatDialogRef,
} from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { ListAuthorsPipe } from '@core/pipes/list-authors.pipe';
import { BookStatus } from '@shared/enum/book-status.enum';
import { IBook } from '@shared/models/book.model';

@Component({
  selector: 'app-add-to-bookshelf',
  standalone: true,
  imports: [
    CommonModule,
    MatDialogActions,
    MatDialogClose,
    MatDialogContent,
    MatButtonModule,
    ListAuthorsPipe,
    ReactiveFormsModule,
    MatFormFieldModule,
    FormsModule,
    MatInputModule,
    MatSelectModule,
    MatIconModule,
  ],
  templateUrl: './add-to-bookshelf.component.html',
  styleUrl: './add-to-bookshelf.component.scss',
})
export class AddToBookshelfComponent {
  bookshelfData = new FormGroup({
    notes: new FormControl<string>(''),
    status: new FormControl<BookStatus | null>(null, Validators.required),
  });

  bookStatus = BookStatus;

  constructor(
    @Inject(MAT_DIALOG_DATA) public book: IBook,
    private dialogRef: MatDialogRef<AddToBookshelfComponent>,
  ) {}

  addBook(): void {
    // oznaczam by walidacja pola weszła
    this.bookshelfData.markAllAsTouched();

    if (this.bookshelfData.valid) {
      this.dialogRef.close(this.bookshelfData.value);
    }
  }
}
