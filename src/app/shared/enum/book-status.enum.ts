export enum BookStatus {
  READ = 'READ',
  NOT_READ = 'NOT READ',
  TO_READ = 'AWAITS TO BE READ',
}
