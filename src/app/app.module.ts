import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatToolbarModule } from '@angular/material/toolbar';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { LoaderComponent } from './core/components/loader/loader.component';
import { ToolbarComponent } from './core/components/toolbar/toolbar.component';
import { ApiInterceptor } from './core/interceptors/api.interceptor';

const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('./modules/book-list/book-list.module').then(
        (m) => m.BookListModule,
      ),
  },
  {
    path: 'details/:id',
    loadChildren: () =>
      import('./modules/book-details/book-details.module').then(
        (m) => m.BookDetailsModule,
      ),
  },
  {
    path: 'bookshelf',
    loadChildren: () =>
      import('./modules/bookshelf/bookshelf.module').then(
        (m) => m.BookshelfModule,
      ),
  },
];

@NgModule({
  declarations: [AppComponent],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    BrowserAnimationsModule,
    HttpClientModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    ToolbarComponent,
    LoaderComponent,
    MatDialogModule,
    MatSnackBarModule,
  ],
})
export class AppModule {}
